<?php
$seo = new rex_yrewrite_seo();
?>

<base href="<?php echo rexx::getBaseUrl(); ?>" />
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">

<meta name="theme-color" content="#ffffff">

    <title><?php echo rexx::getTitle(); ?></title>
    
    <meta name="description" content="<?php echo rexx::getDescription(); ?>" />
    <meta name="keywords" content="<?php echo rexx::getKeywords(); ?>" />
    <meta name="robots" content="<?php echo rexx::getRobotRules();?>" />
        
    <link rel="canonical" href="<?php echo rexx::getCanonicalUrl(); ?>" />
<!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="<?=massif_minify::getCSSFile("style.scss")?>" type="text/css" />
    