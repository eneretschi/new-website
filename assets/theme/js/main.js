$(document).ready(function(){
	$('[data-toggle="tooltip"]').tooltip(); 
	$(".rslides").responsiveSlides();
	if($(window).width() >= 992){
		$('.columnContainer').mouseover(function(){

			var imgref = $(this).attr('imgref')
			$('.bannerRow').each(function(){
					if($(this).attr('imgref')==imgref){
						
						$(this).show();
						$(this).addClass('activ');
					}else{
						$(this).removeClass('activ');
						setTimeout(function(){$(this).hide()},1500)
					}
			});
		});
		$('#startOverview').mouseleave(function(){
			var imgref = 0;
			$('.bannerRow').each(function(){
					if($(this).attr('imgref')==imgref){

						$(this).show();
						$(this).addClass('activ');
					}else{
						$(this).removeClass('activ');
						setTimeout(function(){$(this).hide()},1500)
					}
			});
		});
	}else {
		$('.columnContainer a').click(function(e){
			if($(this).find(".columnContent").hasClass('activ')){
				$(this).find(".columnContent").removeClass('activ');
				$(this).find(".columnContent").slideUp();

			}else{
				$(".columnContent").each(function(){
					$(this).slideUp();
					$(this).removeClass('activ');
				});
				$(this).find(".columnContent").addClass('activ');
				$(this).find(".columnContent").slideDown();
				

			e.preventDefault();
			}
		});
	}


	$('#navSub li a').hover(function(){
		$(this).parent().parent().parent().addClass($(this).attr('colorclass'));
	}, function(){
		$(this).parent().parent().parent().removeClass($(this).attr('colorclass'));
	});

	$('.filterCheck').change(function(){
		//alert($(this).is(':checked'));
		if($(this).is(':checked')){
			var currentCheck = $(this).attr('value');
			$('.wohnungenTable .tableBody span').each(function(){
				if($(this).html() == currentCheck){
					$(this).parent().parent().parent().show();
				}
			});
		}else{
			var currentCheck = $(this).attr('value');
			$('.wohnungenTable .tableBody span').each(function(){
				if($(this).html() == currentCheck){
					$(this).parent().parent().parent().hide();
				}
			});
		}

	});

	$(window).resize(function(){
		if($(window).width() > 820){ 

		$("#navigation_container").removeClass("mobActiv");
		}
	});

	$('#navButton').click(function(){
		if($("#navigation_container").hasClass('mobActiv')){

		$("#navigation_container").removeClass("mobActiv");
		$("#navigation_container").slideUp();
		}else{
		$("#navigation_container").addClass("mobActiv");
		$("#navigation_container").slideDown();
	}
	});
var objektKategorie = '';
	$('#navSub ul li').each(function(){
			if($(this).find('a').hasClass('activ')){

	
				if($(this).hasClass('colorYellow')){
					objektKategorie = 'Wohneigentum';

				}else if($(this).hasClass('colorRosa')){

					objektKategorie = 'Mietwohnungen für Senioren';

				}else if($(this).hasClass('colorGreen')){

					objektKategorie = 'Mietwohnungen';
				}
			}
		});
	$('.wohnungButtonContainer .kontaktLink a').attr('href', '/kontakt/?wohnkategorie='+objektKategorie);

	var objektId = getVariable('objektId');
	var objektKategorie = getVariable('wohnkategorie');
	if(objektKategorie != ''){
	var objektKategorie = objektKategorie.replace(/%20/g, " ");
	var objektKategorie = objektKategorie.replace(/%C3%BC/g, "ü");

	}

	if(objektKategorie != ""){
		$('.formcheckbox').each(function(){
			if($(this).find('label').find('input').val() == objektKategorie){
					if(objektId != ""){
					$('#yform-formular-objekt').find('input').insertAfter($(this).find('label'));
				$(this).find('label').after('<span>&nbsp;Objekt:&nbsp;</span> ');
					
					$(this).find('.form-control').val(objektId);
}else {
	$('#yform-formular-objekt').hide();
}
					$(this).find('label').find('input').attr('checked', '')
					//$(this).find('label').after(' (Objekt: '+objektId+')');
			}
		});
	}else {
		$('#yform-formular-objekt').hide();
	}
});

function getVariable(variable)
{
       var query = window.location.search.substring(1);
       var vars = query.split("&");
       for (var i=0;i<vars.length;i++) {
               var pair = vars[i].split("=");
               if(pair[0] == variable){return pair[1];}
       }
       return(false);
}