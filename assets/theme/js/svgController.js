
// animationsdauer
var duration = 250;
// verschiebung eines geschosses in pixel
var geschossYmoveAmount = 300;

// die auslöser der animation
var $controls = $('.wohnungenTable .tableRow > a');

// über alle anker iterieren
$controls.each(function(){
	
	// element cache
	var $el = $(this);
	// aus dem data attribut wird die hover farbe geholt
	var activeColor = $el.data('color');
	
	// id der wohnung aus dem data attribut
	var wohnungId = $el.data('id');
	//console.log(wohnungId);
	// svg objekt wird anhand der id referenziert -> svg.js
	var svgWohnung = SVG.get(wohnungId);
	
	// der wohnungs-id wird geschoss id, geschoss etage/level und haus id entnommen
	var geschossId = wohnungId.substr(0,2);
	var geschossLevel = geschossId.substr(1,1);
	var hausId = wohnungId.substr(0,1) + '_Haus';
	
	// alle geschosse des hauses auslesen, die sich über der gewählten wohnung befinden
	var $geschosseAll = $('#' + hausId).children();
	var $geschosse = $('#' + hausId).children().filter(function(){
		var level = $(this).attr('id').substr(1,1);
		if(level > geschossLevel) {
			return true;
		} 
		return false;
	});	

/*if($('section.wohnungsOverview').hasClass('themeWohneigentum')){

		$('#A_Haus').children().each(function(){
			var current_geschoss = $(this).attr('id');
			// svg objekt des jeweiligen geschosses -> svg.js
			var svgGeschoss = SVG.get(current_geschoss);

			$('#' + current_geschoss).children().each(function(){
				var svgCurrentWohnung = SVG.get($(this).attr('id'));
				svgCurrentWohnung.each(function(){
			// füllfarbe in ursprungsfarbe animieren
			console.log();
			this.addClass('grayscale');
				});
				 
			});

			// geschoss transformation zurück zur originalposition animieren -> svg.js
			svgGeschoss.animate(duration).move(0, 0);
		});

	}*/
	
	// hover event ...
	$el.click(function(){
		var objektId = $(this).first().find('span').html();

		var objektKategorie = '';
	$('#navSub ul li').each(function(){
			if($(this).find('a').hasClass('activ')){

	
				if($(this).hasClass('colorYellow')){
					objektKategorie = 'Wohneigentum';

				}else if($(this).hasClass('colorRosa')){

					objektKategorie = 'Mietwohnungen für Senioren';

				}else if($(this).hasClass('colorGreen')){

					objektKategorie = 'Mietwohnungen';
				}
			}
		});

		$('.wohnungButtonContainer .kontaktLink a').attr('href', '/kontakt/?objektId='+objektId+'&wohnkategorie='+objektKategorie);

		if($(window).width() <= 768 && $(this).parent().find('.pdfDownload').find('a').attr('href')){

			window.open($(this).parent().find('.pdfDownload').find('a').attr('href'));
		}
		$('.contentRow').removeClass('activ');
		$(this).parent().addClass('activ');
		// über alle geschosse iterieren, die über der gewählten wohnung liegen
		$geschosseAll.each(function(){
			var current_geschoss = $(this).attr('id');
			// svg objekt des jeweiligen geschosses -> svg.js
			var svgGeschoss = SVG.get(current_geschoss);

			$('#' + current_geschoss).children().each(function(){
				var svgCurrentWohnung = SVG.get($(this).attr('id'));
				svgCurrentWohnung.each(function(){
					this.animate(duration).attr({ fill: $(this).data('fill') });
			// füllfarbe in ursprungsfarbe animieren
				});
				svgCurrentWohnung.animate(duration).attr({ fill: $(this).data('fill') });
			});

			// geschoss transformation zurück zur originalposition animieren -> svg.js
			svgGeschoss.animate(duration).move(0, 0);
		});

		
		
		// untere zeile auskommentieren um zu sehen, ob alles korrekt ermittelt wurde
		//console.log('Wohnung ID: ' + wohnungId, 'Geschoss ID: ' + geschossId, 'Geschoss Level: ' + geschossLevel,  'Haus ID: ' + hausId);

		setTimeout(function(){
		// über alle geschosse iterieren, die über der gewählten wohnung liegen
		$geschosse.each(function(){
			// svg objekt des jeweiligen geschosses -> svg.js
			var svgGeschoss = SVG.get($(this).attr('id'));
			// animation zurücksetzen -> svg.js
			svgGeschoss.stop();
			// geschoss transformation animieren -> svg.js
			svgGeschoss.animate(duration).move(0, -geschossYmoveAmount);
		});
		
		// über alle fariben flächen der gewählten wohnung iterieren -> svg.js (speziells each, siehe svg.js doku)
		svgWohnung.each(function(){
			// ursprungsfarbe bzw. fill zwischenspeichern
			$(this).data('fill', $(this.node).attr('fill'));
			// füllfarbe animieren
			this.animate(duration).attr({ fill: activeColor });
		});


			
		}, duration)

	});
	
});

// PRESTO! :) 
// Gruss Yves
