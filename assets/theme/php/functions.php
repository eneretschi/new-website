<?php
	
	class rex_tmpl {

		
		function getBlock($block) {
	  	
	  	return include(rex_path::frontend('/theme/_templates/blocks/' . $block . '.php'));
	  
	  }
	  
	   
	  
	  
	  function getModule($module) {
	  	
	  	return include(rex_path::frontend('/theme/_modules/' . $module . '.php'));
	  
	  }
	  
	  
	  
	  
	  function getArticle($ctype, $article_id = 'this', $fallback = NULL) {
		  
		  
		  // Current Article
		  
		  if ($article_id == 'this') {
			  
			  $article_output = rex_var_article::getArticle(rex_article::getCurrentId(), $ctype, rex_clang::getCurrentId());
			  
			}
			else {
			
				$article_output = rex_var_article::getArticle($article_id, $ctype, rex_clang::getCurrentId());
				
			}
			
			
			// Fallback
		  
		  if ($article_output == '' && $fallback != NULL) {
			  $article_output = rex_var_article::getArticle($fallback, $ctype, rex_clang::getCurrentId());
			}

			
			return $article_output;
		 
	  }
	  
	  
	}
	
?>